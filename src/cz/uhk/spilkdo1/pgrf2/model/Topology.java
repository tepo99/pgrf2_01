package cz.uhk.spilkdo1.pgrf2.model;

import java.util.ArrayList;
import java.util.List;

public class Topology {
    List<Integer> indexes = new ArrayList<>();
    List<Part> parts = new ArrayList<>();

    public List<Integer> getIndexes() {
        return indexes;
    }

    public List<Part> getParts() {
        return parts;
    }
}
