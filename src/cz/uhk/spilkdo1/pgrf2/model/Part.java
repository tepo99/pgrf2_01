package cz.uhk.spilkdo1.pgrf2.model;

public class Part {

    private PartType type;
    private int index;
    private int count;

    public Part(PartType type, int index, int count) {
        this.type = type;
        this.index = index;
        this.count = count;
    }

    public PartType getType() {
        return type;
    }

    public int getIndex() {
        return index;
    }

    public int getCount() {
        return count;
    }
}
