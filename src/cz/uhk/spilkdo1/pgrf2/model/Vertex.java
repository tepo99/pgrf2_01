package cz.uhk.spilkdo1.pgrf2.model;

import transforms.*;

public class Vertex implements Vectorable {
    Point3D position;
    Vec2D uv;
    Col color;
    double one;

    public Vec2D getUv() {
        return uv;
    }


    public Point3D getPosition() {
        return position;
    }

    public Col getColor() {
        return color.mul(1.0/one);
    }

    public Vertex(Point3D position, Col color, Vec2D uv) {
        this.position = position;
        this.color = color;
        this.uv = uv;
        this.one = 1;
    }
    public Vertex(Point3D position, Col color, Vec2D uv, double one){
        this.position = position;
        this.color = color;
        this.uv = uv;
        this.one = one;
    }

    @Override
    public Vertex mul(double k) {
        return new Vertex(this.position.mul(k), this.color.mul(k), this.uv.mul(k),one * k);
    }

    @Override
    public Vertex mul(Mat4 mat) {
        return new Vertex(position.mul(mat), color, uv, one);
    }

    @Override
    public Vertex add(Vertex v) {
        return new Vertex(this.position.add(v.position), this.color.add(v.color), this.uv.add(v.uv), this.one + v.one);
    }

    public double getOne() {
        return one;
    }

    public Vertex dehomog() {
        if(position.dehomog().isPresent())
            return new Vertex(new Point3D(position.dehomog().get()), this.color.mul(1/position.getW()), this.uv.mul(1/position.getW()), this.one/position.getW());
        return null;
    }

    public Vertex withPosition(Point3D position) {
        return new Vertex(position, this.color, this.uv, this.one);
    }

    public Vertex withPosition(Vec3D position) {
        return new Vertex(new Point3D(position), this.color, this.uv, this.one);
    }

//    public static Vertex interpolate(Vertex a, Vertex b, double t) {
//        Col color = new Col(a.getColor().getR()*(1-t) + b.getColor().getR()*t,
//                a.getColor().getG() * (1-t) + b.getColor().getG()*t,
//                a.getColor().getB() * (1-t) + b.getColor().getB()*t,
//                a.getColor().getA()*(1-t) + b.getColor().getB()*t
//         );
//        return new Vertex(
//                a.getPosition().mul(1-t).add(b.getPosition().mul(t)),
//                color,
//                a.getOne()*(1-t) + b.getOne()*t
//        );
//    }

    public static Vertex interpolate(Vertex a, Vertex b, double t) {
        return a.mul(1.0-t).add(b.mul(t));
    }

}
