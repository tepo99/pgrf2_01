package cz.uhk.spilkdo1.pgrf2.model;

import java.util.ArrayList;
import java.util.List;

public class Geometry {
    List<Vertex> vertices = new ArrayList<>();

    public List<Vertex> getVertices(){
        return vertices;
    }

    public void add(Vertex v){
        vertices.add(v);
    }
}
