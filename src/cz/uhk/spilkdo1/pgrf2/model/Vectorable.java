package cz.uhk.spilkdo1.pgrf2.model;

import transforms.Mat4;

public interface Vectorable {

    Vertex mul(double k);
    Vertex mul(Mat4 mat);
    Vertex add(Vertex v);
}
