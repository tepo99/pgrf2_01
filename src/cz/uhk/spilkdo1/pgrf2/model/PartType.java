package cz.uhk.spilkdo1.pgrf2.model;

public enum PartType {
    VERTEXES, LINES, LINE_STRIP, LINE_LOOP, TRIANGLE, TRIANGLE_STRIP, TRIANGLE_FAN
}
