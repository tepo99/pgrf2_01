package cz.uhk.spilkdo1.pgrf2.scene;

import cz.uhk.spilkdo1.pgrf2.solids.*;
import transforms.Mat4Scale;
import transforms.Mat4Transl;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class SolidsList extends AbstractListModel<Solid> {
    private List<Solid> solids = new ArrayList<>();

    public SolidsList() {
        solids.add(new AxisMarker());
        Solid floorGrid = new Grid(-10, -10, 10, 10, 0.5, 0x555555, false);
        solids.add(floorGrid);
        Solid tetrahedron = new Tetrahedron();
        tetrahedron.transform(new Mat4Scale(1.5));
        tetrahedron.transform(new Mat4Transl(0.2, 0.2, 0));
        solids.add(tetrahedron);
        solids.add(new Cube());
    }

    @Override
    public int getSize() {
        return solids.size() - 2;
    }

    @Override
    public Solid getElementAt(int i) {
        return solids.get(i + 2);
    }

    public List<Solid> getList(){
        return solids;
    }

    public void add(Solid solid) {
        solids.add(solid);
    }

    public void remove(int i) {
        System.out.println("Removing solid " + i);
        solids.remove(i);
    }
}
