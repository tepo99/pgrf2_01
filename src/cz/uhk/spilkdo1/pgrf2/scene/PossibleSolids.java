package cz.uhk.spilkdo1.pgrf2.scene;

import cz.uhk.spilkdo1.pgrf2.solids.SolidType;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class PossibleSolids extends AbstractListModel<SolidType> {
    private List<SolidType> solidTypes;

    public PossibleSolids() {
        solidTypes = Arrays.asList(SolidType.values());
    }

    @Override
    public int getSize() {
        return solidTypes.size();
    }

    @Override
    public SolidType getElementAt(int i) {
        return solidTypes.get(i);
    }
}
