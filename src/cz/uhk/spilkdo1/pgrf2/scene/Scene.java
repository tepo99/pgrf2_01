package cz.uhk.spilkdo1.pgrf2.scene;

import cz.uhk.spilkdo1.pgrf2.renderers.RenderType;
import cz.uhk.spilkdo1.pgrf2.shaders.ConstantShader;
import cz.uhk.spilkdo1.pgrf2.shaders.RainbowShader;
import cz.uhk.spilkdo1.pgrf2.shaders.Shader;
import cz.uhk.spilkdo1.pgrf2.shaders.TextureShader;
import cz.uhk.spilkdo1.pgrf2.solids.*;
import transforms.*;


public class Scene {
    private Mat4 projection;
    private Mat4 view;
    private SolidsList solids = new SolidsList();
    private RenderType renderType;
    private boolean cameraMovement = false, objectMovement = false;
    private int shaderType = 0;
    private Shader shader = new RainbowShader();

    public Scene(int width, int height) {
        this.projection = new Mat4PerspRH(Math.toRadians(60), height/(double) width, 0.5, 40);
        this.view = new Mat4Identity();
        this.renderType = RenderType.SOLID;
    }

    public Mat4 getProjection() {
        return projection;
    }

    public Mat4 getView() {
        return view;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public SolidsList getSolids() {
        return solids;
    }

    public RenderType getRenderType() {
        return renderType;
    }

    public void setRenderType(RenderType renderType) {
        this.renderType = renderType;
    }

    public void addSolid(SolidType solidType) {
        switch (solidType) {
            case CUBE:
                solids.add(new Cube());
                break;
            case TETRAHEDRON:
                solids.add(new Tetrahedron());
                break;
            case BEZIER_GRID: {
                solids.add(new BezierGrid());
                break;
            }
        }
    }

    public boolean isCameraMovement() {
        return cameraMovement;
    }

    public void setCameraMovement(boolean cameraMovement) {
        this.cameraMovement = cameraMovement;
    }

    public boolean isObjectMovement() {
        return objectMovement;
    }

    public void setObjectMovement(boolean objectMovement) {
        this.objectMovement = objectMovement;
    }

    public void switchToNextShader() {
        if (shaderType == 2) {
            shaderType = 0;
        } else {
            shaderType++;
        }
        System.out.print("Shader type: " + shaderType + ": ");
        updateShader();
    }

    private void updateShader() {
        switch (shaderType){
            case 0: shader = new RainbowShader();
                System.out.println(shader.getClass().getSimpleName()); break;
            case 1: shader = new ConstantShader();
                System.out.println(shader.getClass().getSimpleName());break;
            case 2: shader = new TextureShader();
                System.out.println(shader.getClass().getSimpleName());
                break;
        }

    }

    public Shader getShader() {
        return shader;
    }

    public void deleteSolid(int i) {
        System.out.println("Deleting solid " + i );
        if(i >= 0 && i < solids.getSize()) {
            solids.remove(i + 2);
        }
    }
}
