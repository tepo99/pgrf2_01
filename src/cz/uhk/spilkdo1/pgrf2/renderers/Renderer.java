package cz.uhk.spilkdo1.pgrf2.renderers;

import cz.uhk.spilkdo1.pgrf2.model.Part;
import cz.uhk.spilkdo1.pgrf2.scene.Scene;
import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import cz.uhk.spilkdo1.pgrf2.rasterizers.ShapeRasterizer;
import cz.uhk.spilkdo1.pgrf2.solids.Solid;
import transforms.Mat4;

import java.util.ArrayList;
import java.util.List;

public class Renderer {

    private Scene scene;
    private ShapeRasterizer shapeRasterizer;

    public Renderer(Scene scene, ShapeRasterizer shapeRasterizer) {
        this.scene = scene;
        this.shapeRasterizer = shapeRasterizer;
    }

    public void drawSolid(Solid solid) {
        Mat4 transforms = solid.getTransform().mul(scene.getView()).mul(scene.getProjection());
        List<Vertex> transformedVertices = new ArrayList<>();
        for(Vertex vertex: solid.getGeometry().getVertices()){
            transformedVertices.add(vertex.mul(transforms));
        }
        for (Part p : solid.getTopology().getParts()) {
            switch (p.getType()) {
                case LINES:
                    for(int i = 0; i < p.getCount(); i++)
                    drawLine(transformedVertices.get(solid.getTopology().getIndexes().get(2* i)), transformedVertices.get(solid.getTopology().getIndexes().get(2 * i+1)));
                    break;
                case TRIANGLE:
                    for (int i = 0; i < p.getCount(); i++) {
                        drawTriangle(
                                transformedVertices.get(solid.getTopology().getIndexes().get(i * 3 + p.getIndex())),
                                transformedVertices.get(solid.getTopology().getIndexes().get(i * 3 + p.getIndex() + 1)),
                                transformedVertices.get(solid.getTopology().getIndexes().get(i * 3 + p.getIndex() + 2))
                        );
                    }
                    break;
                case TRIANGLE_STRIP:
                    for (int i = 0; i < p.getCount(); i++) {
                        drawTriangle(
                                transformedVertices.get(solid.getTopology().getIndexes().get(i + p.getIndex())),
                                transformedVertices.get(solid.getTopology().getIndexes().get(i + p.getIndex() + 1)),
                                transformedVertices.get(solid.getTopology().getIndexes().get(i + p.getIndex() + 2))
                        );
                    }
                    break;
                default:
                    return;
            }
        }
    }

    private void drawLine(Vertex a, Vertex b) {
        if(notVisible(a) && notVisible(b)) return;
        if(a.getPosition().getZ() < b.getPosition().getZ()) {
            Vertex hold = a;
            a = b;
            b = hold;
        }
        if(a.getPosition().getZ() <= 0) {
            return;
        }
        if(b.getPosition().getZ() <= 0) {
            Vertex ab = interpolate(a, b);
            shapeRasterizer.rasterizeLine(a, ab);
            return;
        }
        shapeRasterizer.rasterizeLine(a, b);
    }

    private void drawTriangle(Vertex a, Vertex b, Vertex c) {
        if(notVisible(a) && notVisible(b) && notVisible(c)){
            return;
        }
        if(a.getPosition().getZ() < b.getPosition().getZ()) {
            Vertex hold = a;
            a = b;
            b = hold;
        }
        if(a.getPosition().getZ() < c.getPosition().getZ()) {
            Vertex hold  = a;
            a = b;
            b = hold;
        }
        if(b.getPosition().getZ() < c.getPosition().getZ()) {
            Vertex hold = b;
            b = c;
            c = hold;
        }
        if(a.getPosition().getZ() <= 0) {
            return;
        }
        if(b.getPosition().getZ() <= 0) {
            Vertex ab = interpolate(a, b);
            Vertex ac = interpolate(a, c);
            if(scene.getRenderType() == RenderType.SOLID) {
                shapeRasterizer.rasterizeTriangle(a, ab, ac);
            } else if (scene.getRenderType() == RenderType.WIREFRAME) {
                shapeRasterizer.rasterizeLine(a, ab);
                shapeRasterizer.rasterizeLine(a, ac);
            }
            return;
        }
        if(c.getPosition().getZ() <= 0) {
            Vertex bc = interpolate(b, c);
            Vertex ac = interpolate(a, c);
            if(scene.getRenderType() == RenderType.SOLID) {
                shapeRasterizer.rasterizeTriangle(a, b, bc);
                shapeRasterizer.rasterizeTriangle(a, bc, ac);
            } else if(scene.getRenderType() == RenderType.WIREFRAME){
                shapeRasterizer.rasterizeLine(a, b);
                shapeRasterizer.rasterizeLine(b, bc);
                shapeRasterizer.rasterizeLine(a, ac);
            }
            return;
        }
        if(scene.getRenderType() == RenderType.SOLID) {
            shapeRasterizer.rasterizeTriangle(a, b, c);
        } else if (scene.getRenderType() == RenderType.WIREFRAME) {
            shapeRasterizer.rasterizeLine(a, b);
            shapeRasterizer.rasterizeLine(a, c);
            shapeRasterizer.rasterizeLine(b, c);
        }
    }

    private Vertex interpolate(Vertex a, Vertex b) {
        double t = (0.0 - a.getPosition().getZ())/(b.getPosition().getZ() - a.getPosition().getZ());
        return a.mul(1.0-t).add(b.mul(t));
    }

    private boolean notVisible(Vertex a) {
        double w = a.getPosition().getW();
        return  a.getPosition().getX() < -w ||
                a.getPosition().getY() < -w ||
                a.getPosition().getZ() < 0 ||
                a.getPosition().getX() > w ||
                a.getPosition().getY() > w ||
                a.getPosition().getZ() > w;
    }
}
