package cz.uhk.spilkdo1.pgrf2.renderers;

public enum RenderType {
    SOLID, WIREFRAME
}
