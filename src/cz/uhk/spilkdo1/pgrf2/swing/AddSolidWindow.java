package cz.uhk.spilkdo1.pgrf2.swing;

import cz.uhk.spilkdo1.pgrf2.scene.PossibleSolids;
import cz.uhk.spilkdo1.pgrf2.solids.SolidType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddSolidWindow extends JFrame {
    private final JList<SolidType> jList;
    private final PossibleSolids possibleSolids;

    /**
     * Otevře okno pro přidání nového solidu.
     *
     * @param canvas CanvasMouse, do kterého se má solid přidat
     */
    public AddSolidWindow(ImageWindow canvas) {
        possibleSolids = new PossibleSolids();
        jList = new JList<>(possibleSolids);
        jList.setPreferredSize(new Dimension(300, 20 * possibleSolids.getSize()));
        JButton addSolidButton = new JButton("Přidat");
        setTitle("Přidat těleso");
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        add(jList, BorderLayout.CENTER);
        add(addSolidButton, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);

        addSolidButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                canvas.getScene().addSolid(possibleSolids.getElementAt(jList.getSelectedIndex()));
                canvas.redraw();
                dispose();
            }
        });
    }
}
