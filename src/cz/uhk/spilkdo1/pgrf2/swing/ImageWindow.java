package cz.uhk.spilkdo1.pgrf2.swing;

import cz.uhk.spilkdo1.pgrf2.controllers.KeyboardController;
import cz.uhk.spilkdo1.pgrf2.controllers.MouseController;
import cz.uhk.spilkdo1.pgrf2.scene.Scene;
import cz.uhk.spilkdo1.pgrf2.rasterizers.ShapeRasterizer;
import cz.uhk.spilkdo1.pgrf2.rasters.ImageBuffer;
import cz.uhk.spilkdo1.pgrf2.rasters.VisibilityBuffer;
import cz.uhk.spilkdo1.pgrf2.renderers.Renderer;
import cz.uhk.spilkdo1.pgrf2.solids.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class ImageWindow {

    private final MouseController mouseController;
    private JPanel panel;
    private JList sceneSolidsList;
    int width, height;
    private ImageBuffer imageBuffer;
    private VisibilityBuffer visibilityBuffer;
    private Renderer renderer;
    private Scene scene;
    private ShapeRasterizer rasterizer;

    public ImageWindow(int width, int height) {
        this.width = width;
        this.height = height;
        JFrame frame = new JFrame();
        frame.setLayout(new BorderLayout());
        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        imageBuffer = new ImageBuffer(width - 100, height);
        visibilityBuffer = new VisibilityBuffer(width - 100, height);

        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };
        initObjects();
        panel.setPreferredSize(new Dimension(width - 100, height));
        panel.setFocusable(true);
        panel.requestFocusInWindow();
        panel.requestFocus();
        frame.add(panel, BorderLayout.CENTER);

        sceneSolidsList = new JList<>(scene.getSolids());
        sceneSolidsList.setPreferredSize(new Dimension(100, height));
        sceneSolidsList.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                panel.grabFocus();
            }
        });
        frame.add(sceneSolidsList, BorderLayout.EAST);
        frame.pack();
        frame.setVisible(true);

        mouseController = new MouseController(this);
        panel.addMouseListener(mouseController);
        panel.addMouseMotionListener(mouseController);
        panel.addMouseWheelListener(mouseController);
        KeyboardController keyboardController = new KeyboardController(this);
        panel.addKeyListener(keyboardController);

        redraw();
    }

    public void redraw() {
        clear();
        rasterizer.setShader(scene.getShader());
        for(Solid solid: scene.getSolids().getList()){
            solid.recalc();
            renderer.drawSolid(solid);
        }
        panel.repaint();
        sceneSolidsList.updateUI();
    }

    private void initObjects() {
        scene = new Scene(width, height);
        rasterizer = new ShapeRasterizer(width, height, imageBuffer, visibilityBuffer, scene.getShader());
        renderer = new Renderer(scene, rasterizer);
    }

    private void present(Graphics graphics) {
        graphics.drawImage(imageBuffer.getBuffer(), 0, 0, null);
    }

    public void clear() {
        imageBuffer.clear();
        visibilityBuffer.clear();
        Graphics gr = imageBuffer.getBuffer().getGraphics();
        gr.setColor(new Color(0x2f2f2f));
        gr.fillRect(0, 0, width, height);
    }

    public Scene getScene(){
        return scene;
    }

    public void invokeAddSolidsWindow() {
        SwingUtilities.invokeLater(() -> new AddSolidWindow(this));
    }

    public int getSelectedSolidIndex(){
        return sceneSolidsList.getSelectedIndex();
    }

    public MouseController getMouseController() {
        return mouseController;
    }
}
