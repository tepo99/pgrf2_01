package cz.uhk.spilkdo1.pgrf2.shaders;

import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;

@FunctionalInterface
public interface Shader {

    Col getColor(Vertex v);
}
