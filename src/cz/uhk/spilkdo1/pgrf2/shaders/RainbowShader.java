package cz.uhk.spilkdo1.pgrf2.shaders;

import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;

public class RainbowShader implements Shader {
    @Override
    public Col getColor(Vertex v) {
        return v.getColor();
    }
}
