package cz.uhk.spilkdo1.pgrf2.shaders;

import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;

public class TextureShader implements Shader{
    @Override
    public Col getColor(Vertex v) {
        double uvx = (v.getUv().getX()/v.getOne()) * 10;
        double uvy = (v.getUv().getY()/v.getOne()) * 10;
//        System.out.println(uvy + " " +uvy % 5);
        if(uvy % 2 < 1 || uvx % 2 < 1) return new Col(0, 200, 200);
        else return new Col(0, 0, 255);
    }
}
