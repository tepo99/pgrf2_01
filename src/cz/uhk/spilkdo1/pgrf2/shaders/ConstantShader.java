package cz.uhk.spilkdo1.pgrf2.shaders;

import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;

public class ConstantShader implements Shader {
    @Override
    public Col getColor(Vertex v) {
        return new Col(255, 0, 0);
    }
}
