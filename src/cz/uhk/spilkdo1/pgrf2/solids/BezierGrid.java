package cz.uhk.spilkdo1.pgrf2.solids;

import cz.uhk.spilkdo1.pgrf2.model.Part;
import cz.uhk.spilkdo1.pgrf2.model.PartType;
import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.*;

import java.util.ArrayList;
import java.util.List;

public class BezierGrid extends Solid {
    private final Mat4 bicubicMat;

    public BezierGrid() {
        this.bicubicMat = Cubic.BEZIER;
        recalc();
    }

    @Override
    public void recalc() {
        geometry.getVertices().clear();
        topology.getParts().clear();
        topology.getIndexes().clear();
        List<Point3D> points = new ArrayList<>();
        points.add(new Point3D(0, 0, 0));
        points.add(new Point3D(1, 0, 0.2));
        points.add(new Point3D(2, 0, 0.4));
        points.add(new Point3D(3, 0, 0.6));
        points.add(new Point3D(0, 1, 0.8));
        points.add(new Point3D(1, 1, 1));
        points.add(new Point3D(2, 1, 1.2));
        points.add(new Point3D(3, 1, 1.4));
        points.add(new Point3D(0, 2, 1.6));
        points.add(new Point3D(1, 2, 1.8));
        points.add(new Point3D(2, 2, 2));
        points.add(new Point3D(3, 2, 1.6));
        points.add(new Point3D(0, 3, 1.2));
        points.add(new Point3D(1, 3, 0.8));
        points.add(new Point3D(2, 3, 0.4));
        points.add(new Point3D(3, 3, 0));

        Bicubic bicubic = new Bicubic(bicubicMat, points.toArray(Point3D[]::new));
        for (int i = 0; i <= 5; i++) {
            for (int j = 0; j <= 5; j++) {
                geometry.getVertices().add(new Vertex(bicubic.compute(i / 5.0, j / 5.0), new Col(0x330000 * i + 0x003300 * j), new Vec2D(i/5.0, j/5.0)));
            }
        }
        for (int i = 0; i < 5; i++) {
            topology.getIndexes().add((i+ 1) * 6);
            topology.getIndexes().add(i * 6);
            topology.getIndexes().add((i + 1) * 6+ 1);
            topology.getIndexes().add(i * 6 + 1);
            topology.getIndexes().add((i + 1) * 6+ 2);
            topology.getIndexes().add(i * 6 + 2);
            topology.getIndexes().add((i + 1) * 6+ 3);
            topology.getIndexes().add(i * 6 + 3);
            topology.getIndexes().add((i + 1) * 6+ 4);
            topology.getIndexes().add(i * 6 + 4);
            topology.getIndexes().add((i + 1) * 6+ 5);
            topology.getIndexes().add(i * 6 + 5);
            topology.getParts().add(new Part(PartType.TRIANGLE_STRIP, i * 12, 10));
        }
        //topology.getParts().add(new Part(PartType.LINES, 0, topology.getIndexes().size()/2));
    }

}
