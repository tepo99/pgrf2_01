package cz.uhk.spilkdo1.pgrf2.solids;

import cz.uhk.spilkdo1.pgrf2.model.Part;
import cz.uhk.spilkdo1.pgrf2.model.PartType;
import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec2D;

public class AxisMarker extends Solid {

    public AxisMarker(){
        geometry.add(new Vertex(new Point3D(0, 0, 0), new Col(0xff0000), new Vec2D(0, 0)));
        geometry.add(new Vertex(new Point3D(1, 0, 0), new Col(0xff0000), new Vec2D(0, 0)));
        geometry.add(new Vertex(new Point3D(0, 0, 0), new Col(0x00ff00), new Vec2D(0, 0)));
        geometry.add(new Vertex(new Point3D(0, 1, 0), new Col(0x00ff00), new Vec2D(0, 0)));
        geometry.add(new Vertex(new Point3D(0, 0, 0), new Col(0x0000ff), new Vec2D(0, 0)));
        geometry.add(new Vertex(new Point3D(0, 0, 1), new Col(0x0000ff), new Vec2D(0, 0)));

        topology.getParts().add(new Part(PartType.LINES, 0, 3));
        topology.getIndexes().add(0);
        topology.getIndexes().add(1);
        topology.getIndexes().add(2);
        topology.getIndexes().add(3);
        topology.getIndexes().add(4);
        topology.getIndexes().add(5);
    }

    @Override
    public void transform(Mat4 transform) {
    }
}
