package cz.uhk.spilkdo1.pgrf2.solids;

import cz.uhk.spilkdo1.pgrf2.model.Part;
import cz.uhk.spilkdo1.pgrf2.model.PartType;
import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;
import transforms.Point3D;
import transforms.Vec2D;

public class Cube extends Solid{
    public Cube() {
        geometry.getVertices().add(new Vertex(new Point3D(0, 0, 0), new Col(0xff0000), new Vec2D(0, 0)));
        geometry.getVertices().add(new Vertex(new Point3D(1, 0, 0), new Col(0x00ff00), new Vec2D(0, 1)));
        geometry.getVertices().add(new Vertex(new Point3D(1, 1, 0), new Col(0x0000ff), new Vec2D(1, 1)));
        geometry.getVertices().add(new Vertex(new Point3D(0, 1, 0), new Col(0xffff00), new Vec2D(1, 0)));
        geometry.getVertices().add(new Vertex(new Point3D(0, 0, 1), new Col(0x00ff00), new Vec2D(1, 0)));
        geometry.getVertices().add(new Vertex(new Point3D(1, 0, 1), new Col(0xff00ff), new Vec2D(1, 1)));
        geometry.getVertices().add(new Vertex(new Point3D(1, 1, 1), new Col(0xffffff), new Vec2D(0, 1)));
        geometry.getVertices().add(new Vertex(new Point3D(0, 1, 1), new Col(0x000000), new Vec2D(0, 0)));

        topology.getParts().add(new Part(PartType.TRIANGLE_STRIP, 0, 8));
        topology.getIndexes().add(1);
        topology.getIndexes().add(0);
        topology.getIndexes().add(2);
        topology.getIndexes().add(3);
        topology.getIndexes().add(6);
        topology.getIndexes().add(7);
        topology.getIndexes().add(5);
        topology.getIndexes().add(4);
        topology.getIndexes().add(1);
        topology.getIndexes().add(0);

        topology.getParts().add(new Part(PartType.TRIANGLE_STRIP, 10, 2));
        topology.getIndexes().add(0);
        topology.getIndexes().add(4);
        topology.getIndexes().add(3);
        topology.getIndexes().add(7);
//
        topology.getParts().add(new Part(PartType.TRIANGLE_STRIP, 14, 2));
        topology.getIndexes().add(1);
        topology.getIndexes().add(2);
        topology.getIndexes().add(5);
        topology.getIndexes().add(6);
    }


}
