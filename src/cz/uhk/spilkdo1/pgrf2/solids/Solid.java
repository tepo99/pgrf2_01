package cz.uhk.spilkdo1.pgrf2.solids;

import cz.uhk.spilkdo1.pgrf2.model.Geometry;
import cz.uhk.spilkdo1.pgrf2.model.Topology;
import transforms.Mat4;
import transforms.Mat4Identity;

public abstract class Solid {
    Geometry geometry = new Geometry();
    Topology topology = new Topology();
    Mat4 transform = new Mat4Identity();

    boolean isEditable = true;

    public Geometry getGeometry() {
        return geometry;
    }

    public Topology getTopology() {
        return topology;
    }

    public Mat4 getTransform() {
        return transform;
    }

    public void transform(Mat4 transform) {
        this.transform= transform.mul(this.transform);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    public void recalc(){

    }
}
