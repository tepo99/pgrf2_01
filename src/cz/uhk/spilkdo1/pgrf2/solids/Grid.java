package cz.uhk.spilkdo1.pgrf2.solids;

import cz.uhk.spilkdo1.pgrf2.model.Part;
import cz.uhk.spilkdo1.pgrf2.model.PartType;
import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec2D;

/**
 * Nový pravidelný grid s výškou 0.
 */
public class Grid extends Solid {

    /**
     * @param minX       minimální hodnota v X směru
     * @param minY       minimální hodnota v Y směru
     * @param maxX       maximální hodnota v X směru
     * @param maxY       maximální hodnota v Y směru
     * @param scale      velikost mříže
     * @param color      barva mříže
     * @param isEditable boolean, zda má být možné vytvořenou mříž upravovat.
     */
    public Grid(int minX, int minY, int maxX, int maxY, double scale, int color, boolean isEditable) {
        this.isEditable = isEditable;
        for (int i = minX; i <= maxX; i++) {
            for (int j = minY; j <= maxY; j++) {
                geometry.getVertices().add(new Vertex(new Point3D(i * scale, j * scale, 0), new Col(color), new Vec2D(0, 0)));
            }
        }

        for (int i = 0; i <= maxX - minX; i++) {
            for (int j = 0; j <= maxY - minY; j++) {
                if (j > 0) {
                    topology.getIndexes().add(i * (maxY - minY + 1) + j - 1);
                    topology.getIndexes().add(i * (maxY - minY + 1) + j);
                }
                if (i > 0) {
                    topology.getIndexes().add((i - 1) * (maxY - minY + 1) + j);
                    topology.getIndexes().add((i) * (maxY - minY + 1) + j);
                }

            }
        }
        topology.getParts().add(new Part(PartType.LINES, 0, (topology.getIndexes().size()) /2));
    }

    @Override
    public void transform(Mat4 transform) {
        if(isEditable) {
            this.transform = transform.mul(this.transform);
        }
    }
}
