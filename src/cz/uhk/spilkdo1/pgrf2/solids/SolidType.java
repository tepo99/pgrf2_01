package cz.uhk.spilkdo1.pgrf2.solids;

public enum SolidType {
    TETRAHEDRON, CUBE, BEZIER_GRID
}
