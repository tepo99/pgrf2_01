package cz.uhk.spilkdo1.pgrf2.solids;

import cz.uhk.spilkdo1.pgrf2.model.Part;
import cz.uhk.spilkdo1.pgrf2.model.PartType;
import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import transforms.Col;
import transforms.Mat4Transl;
import transforms.Point3D;
import transforms.Vec2D;

public class Tetrahedron extends Solid {

    public Tetrahedron(){
        geometry.getVertices().add(new Vertex(new Point3D(0, 0, 0), new Col(0xff0000), new Vec2D(0, 0.2)));
        geometry.getVertices().add(new Vertex(new Point3D(0, 0, 1), new Col(0x00ff00), new Vec2D(0, 0.8)));
        geometry.getVertices().add(new Vertex(new Point3D(1, 0, 0), new Col(0x0000ff), new Vec2D(1, 0.2)));
        geometry.getVertices().add(new Vertex(new Point3D(0, 1, 0), new Col(0xffff00), new Vec2D(1, 0.8)));

        topology.getParts().add(new Part(PartType.TRIANGLE, 0, 4));
        topology.getIndexes().add(0);
        topology.getIndexes().add(1);
        topology.getIndexes().add(2);
        topology.getIndexes().add(0);
        topology.getIndexes().add(2);
        topology.getIndexes().add(3);
        topology.getIndexes().add(0);
        topology.getIndexes().add(1);
        topology.getIndexes().add(3);
        topology.getIndexes().add(1);
        topology.getIndexes().add(2);
        topology.getIndexes().add(3);

        this.transform.mul(new Mat4Transl(0, 0, 0));
    }
}
