package cz.uhk.spilkdo1.pgrf2.rasterizers;

import cz.uhk.spilkdo1.pgrf2.model.Vertex;
import cz.uhk.spilkdo1.pgrf2.rasters.ImageBuffer;
import cz.uhk.spilkdo1.pgrf2.rasters.VisibilityBuffer;
import cz.uhk.spilkdo1.pgrf2.shaders.Shader;
import transforms.Vec3D;

public class ShapeRasterizer {

    private int width, height;
    private ImageBuffer imageBuffer;
    private VisibilityBuffer visibilityBuffer;
    private Shader shader;

    public ShapeRasterizer(int width, int height, ImageBuffer imageBuffer, VisibilityBuffer visibilityBuffer, Shader shader) {
        this.width = width;
        this.height = height;
        this.imageBuffer = imageBuffer;
        this.visibilityBuffer = visibilityBuffer;
        this.shader = shader;
    }

    public void rasterizeTriangle(Vertex a, Vertex b, Vertex c) {
        Vertex ad, bd, cd;
        if (a.getPosition().dehomog().isPresent() && b.getPosition().dehomog().isPresent() && c.getPosition().dehomog().isPresent()) {
            ad = fitToScreen(a.dehomog());
            bd = fitToScreen(b.dehomog());
            cd = fitToScreen(c.dehomog());
        } else return;
        if (ad.getPosition().getY() > bd.getPosition().getY()) {
            Vertex hold = ad;
            ad = bd;
            bd = hold;
        }
        if (ad.getPosition().getY() > cd.getPosition().getY()) {
            Vertex hold = cd;
            cd = ad;
            ad = hold;
        }
        if (bd.getPosition().getY() > cd.getPosition().getY()) {
            Vertex hold = cd;
            cd = bd;
            bd = hold;
        }
        double t = getT(bd, ad, cd);
        Vertex midpoint = ad.mul(1.0 - t).add(cd.mul(t));
        rasterizeTopPart(ad, bd, midpoint);
        rasterizeBottomPart(cd, bd, midpoint);
    }

    private void rasterizeTopPart(Vertex a, Vertex b, Vertex c) {

        for (int i = (int) Math.max(Math.round(a.getPosition().getY()) + 1, 0); i < Math.min(c.getPosition().getY(), height -1); i++) {
            drawLine(a, b, c, i);
        }
    }
    private void rasterizeBottomPart(Vertex a, Vertex b, Vertex c) {

        for (int i = (int) Math.min(Math.round(a.getPosition().getY()) - 1, height -1 ); i > Math.max(c.getPosition().getY(), 0); i--) {
            drawLine(a, b, c, i);
        }
    }

    private void drawLine(Vertex a, Vertex b, Vertex c, int i) {
        double t1 = getT(i, a, c);
        Vertex leftpoint = Vertex.interpolate(a, c, t1);
        double t2 = getT(i, a, b);
        Vertex rightpoint = Vertex.interpolate(a, b, t2);
        if (leftpoint.getPosition().getX() > rightpoint.getPosition().getX()) {
            Vertex hold = rightpoint;
            rightpoint = leftpoint;
            leftpoint = hold;
        }
        for (int j = (int)Math.max((Math.floor(leftpoint.getPosition().getX()) + 1), 0); j <= Math.min(rightpoint.getPosition().getX(), width -1 ) ; j++) {
            double t3 = (j - leftpoint.getPosition().getX()) / (rightpoint.getPosition().getX() - leftpoint.getPosition().getX());
            Vertex actualPoint = Vertex.interpolate(leftpoint, rightpoint, t3);
            if (visibilityBuffer.checkVisibility((int)Math.round(actualPoint.getPosition().getX()), (int) Math.round(actualPoint.getPosition().getY()), actualPoint.getPosition().getZ())) {
                imageBuffer.setElement((int)Math.round(actualPoint.getPosition().getX()), (int) Math.round(actualPoint.getPosition().getY()), shader.getColor(actualPoint));
            }
        }
    }

    private double getT(int midValue, Vertex leftValue, Vertex rightValue) {
        if (leftValue.getPosition().getY() > rightValue.getPosition().getY() - 0.05 && leftValue.getPosition().getY() < rightValue.getPosition().getY() + 0.05) {
            return 1;
        }
        return ((double)midValue - leftValue.getPosition().getY()) / (rightValue.getPosition().getY() - leftValue.getPosition().getY());
    }

    private double getT(Vertex midValue, Vertex leftValue, Vertex rightValue) {
        if (leftValue.getPosition().getY() > rightValue.getPosition().getY() - 0.05 && leftValue.getPosition().getY() < rightValue.getPosition().getY() + 0.05) {
            return 1;
        }
        return (midValue.getPosition().getY() - leftValue.getPosition().getY()) / (rightValue.getPosition().getY() - leftValue.getPosition().getY());
    }

    private Vertex fitToScreen(Vertex v) {
        return v.withPosition(new Vec3D(
                (v.getPosition().getX() + 1) / 2 * (width - 1),
                (-1* v.getPosition().getY() + 1) / 2 * (height - 1),
                v.getPosition().getZ()));
    }

    public void rasterizeLine(Vertex a, Vertex b) {
        Vertex ad, bd;
        if(a.getPosition().dehomog().isPresent() && b.getPosition().dehomog().isPresent()){
            ad = a.dehomog();
            bd = b.dehomog();
        } else return;
        ad = fitToScreen(ad);
        bd = fitToScreen(bd);
        double deltaX = bd.getPosition().getX() - ad.getPosition().getX();
        double deltaY = bd.getPosition().getY() - ad.getPosition().getY();

        if(Math.abs(deltaX) > Math.abs(deltaY)) {
            drawLineHorizontally(ad, bd);
        } else {
            drawLineVertically(ad, bd);
        }

    }

    private void drawLineVertically(Vertex ad, Vertex bd) {
        if(ad.getPosition().getY() > bd.getPosition().getY()) {
            Vertex hold = ad;
            ad = bd;
            bd = hold;
        }

        for(int i = (int) Math.max(ad.getPosition().getY(), 0); i < Math.min(bd.getPosition().getY(), height - 1); i++){
            double t = getT(i, ad, bd);
            Vertex point = Vertex.interpolate(ad, bd, t);
            if(visibilityBuffer.checkVisibility((int)Math.round(point.getPosition().getX()), (int)Math.round(point.getPosition().getY()), point.getPosition().getZ())){
                imageBuffer.setElement((int)Math.round(point.getPosition().getX()), (int)Math.round(point.getPosition().getY()), point.getColor());
            }
        }
    }

    private void drawLineHorizontally(Vertex ad, Vertex bd) {
        if(ad.getPosition().getX() > bd.getPosition().getX()) {
            Vertex hold = ad;
            ad = bd;
            bd = hold;
        }

        for(int i = (int) Math.max(ad.getPosition().getX(), 0); i < Math.min(bd.getPosition().getX(), width -1); i++){
            double t = (i - ad.getPosition().getX()) / (bd.getPosition().getX() - ad.getPosition().getX());
            Vertex point = Vertex.interpolate(ad, bd, t);
            if(visibilityBuffer.checkVisibility((int)Math.round(point.getPosition().getX()), (int)Math.round(point.getPosition().getY()), point.getPosition().getZ())){
                imageBuffer.setElement((int)Math.round(point.getPosition().getX()), (int)Math.round(point.getPosition().getY()), point.getColor());
            }
        }
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }
}
