package cz.uhk.spilkdo1.pgrf2.rasters;

import transforms.Col;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Optional;

public class ImageBuffer implements Raster<Col> {

    private final int width, height;
    private final BufferedImage img;

    public ImageBuffer(int width, int height) {
        this.width = width;
        this.height = height;
        this.img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setElement(int x, int y, Col element) {
        if(isOutOfBounds(x, y)) return;
        img.setRGB(x, y, element.getRGB());
    }


    @Override
    public Optional<Col> getElement(int x, int y) {
        if(isOutOfBounds(x, y)) return Optional.empty();
        return Optional.of(new Col(img.getRGB(x, y)));
    }

    public Image getBuffer() {
        return img;
    }

    public void clear() {
        Graphics gr = img.getGraphics();
        gr.setColor(new Color(0x2f2f2f));
        gr.fillRect(0, 0, width, height);
    }
}
