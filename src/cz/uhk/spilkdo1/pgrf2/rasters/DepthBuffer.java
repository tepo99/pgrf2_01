package cz.uhk.spilkdo1.pgrf2.rasters;

import java.util.Arrays;
import java.util.Optional;

public class DepthBuffer implements Raster<Double> {
    private final int width, height;
    private Double[][] buffer;

    public DepthBuffer(int width, int height) {
        this.width = width;
        this.height = height;
        buffer = new Double[width][height];
        Arrays.setAll(buffer, i -> {Arrays.fill(buffer[i], 1.0); return buffer[i];});
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setElement(int x, int y, Double element) {
        if(isOutOfBounds(x, y)) return;
        buffer[x][y] = element;
    }

    @Override
    public Optional<Double> getElement(int x, int y) {
        if(isOutOfBounds(x, y)) return Optional.empty();
        return Optional.of(buffer[x][y]);
    }

}
