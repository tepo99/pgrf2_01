package cz.uhk.spilkdo1.pgrf2.rasters;

public class VisibilityBuffer {
    //TODO implementuje z-buffer algoritmus pomoci imagebufferu a depthbufferu
    private DepthBuffer depthBuffer;
    private int width, height;

    public VisibilityBuffer(int width, int height) {
        this.depthBuffer = new DepthBuffer(width, height);
        this.width = width;
        this.height = height;
    }

    public boolean checkVisibility(int x, int y, Double z) {
        if(depthBuffer.getElement(x, y).isPresent() && depthBuffer.getElement(x, y).get() <= z) {
            return false;
        }
        depthBuffer.setElement(x, y, z);
        return true;
    }

    public void clear() {
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++){
                depthBuffer.setElement(i, j ,1.0);
            }
        }
    }
}
