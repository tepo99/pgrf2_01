package cz.uhk.spilkdo1.pgrf2.rasters;

import java.util.Optional;

public interface Raster<E> {

    int getWidth();
    int getHeight();

    void setElement(int x, int y, E element);
    Optional<E> getElement(int x, int y);

    default boolean isOutOfBounds(int x, int y) {
        return x < 0 || y < 0 || x >= getWidth() || y >= getHeight();
    }
}
