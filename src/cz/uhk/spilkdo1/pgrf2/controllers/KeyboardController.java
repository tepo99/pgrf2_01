package cz.uhk.spilkdo1.pgrf2.controllers;

import cz.uhk.spilkdo1.pgrf2.renderers.RenderType;
import cz.uhk.spilkdo1.pgrf2.swing.ImageWindow;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyboardController extends KeyAdapter {

    private final ImageWindow canvas;

    public KeyboardController(ImageWindow canvas) {
        this.canvas = canvas;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W: canvas.getScene().setRenderType(RenderType.WIREFRAME);break;
            case KeyEvent.VK_S: canvas.getScene().setRenderType(RenderType.SOLID);break;
            case KeyEvent.VK_A: canvas.invokeAddSolidsWindow();break;
            case KeyEvent.VK_H: canvas.getScene().switchToNextShader();break;
            case KeyEvent.VK_X:
            case KeyEvent.VK_Z:
            case KeyEvent.VK_Y:
                canvas.getMouseController().setEditAxis(e.getKeyChar());break;
            case KeyEvent.VK_SHIFT: canvas.getScene().setCameraMovement(true);break;
            case KeyEvent.VK_CONTROL: canvas.getScene().setObjectMovement(true);break;
            case KeyEvent.VK_DELETE: canvas.getScene().deleteSolid(canvas.getSelectedSolidIndex());canvas.redraw();break;
        }
        canvas.redraw();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch(e.getKeyCode()) {
            case KeyEvent.VK_SHIFT: canvas.getScene().setCameraMovement(false);break;
            case KeyEvent.VK_CONTROL: canvas.getScene().setObjectMovement(false);break;
        }
        canvas.redraw();
    }
}
