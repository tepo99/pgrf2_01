package cz.uhk.spilkdo1.pgrf2.controllers;

import cz.uhk.spilkdo1.pgrf2.solids.Solid;
import cz.uhk.spilkdo1.pgrf2.swing.ImageWindow;
import transforms.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

public class MouseController extends MouseAdapter {
    private final double MOVE_SPEED = 0.01, ROT_SPEED = 0.0025 ,SCROLL_SPEED = 0.3;
    private ImageWindow canvas;
    private Camera camera, originalCamera;
    private Vec2D mouseDragOrigin;
    private boolean rotate = false, translate = false;
    private char editAxis = 'x';

    public MouseController(ImageWindow canvas) {
        this.canvas = canvas;
        camera = new Camera().withPosition(new Vec3D(5, 5, 5)).withAzimuth(1.25 * Math.PI).withZenith(-0.25 * Math.PI).withFirstPerson(true);
        canvas.getScene().setView(camera.getViewMatrix());
    }

    @Override
    public void mousePressed(MouseEvent e) {
            mouseDragOrigin = new Vec2D(e.getX(), e.getY());
            if(e.getButton() == MouseEvent.BUTTON1){
                rotate = true;
            } else if(e.getButton() == MouseEvent.BUTTON3){
                translate = true;
            }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Vec2D mouseDrag = new Vec2D(e.getX() - mouseDragOrigin.getX(), e.getY() - mouseDragOrigin.getY());
        if(canvas.getScene().isCameraMovement()) {
            moveCamera(mouseDrag);
        } else if (canvas.getScene().isObjectMovement()) {
            moveObject(mouseDrag);
        }
        mouseDragOrigin = new Vec2D(e.getX(), e.getY());
    }

    private void moveObject(Vec2D mouseDrag) {
        Solid movedObject = canvas.getScene().getSolids().getElementAt(canvas.getSelectedSolidIndex());
        if(rotate){
            movedObject.transform(new Mat4RotXYZ((editAxis=='x'?1:0)*ROT_SPEED * mouseDrag.getX(), (editAxis=='y'?1:0)*ROT_SPEED * mouseDrag.getX(), (editAxis=='z'?1:0)*ROT_SPEED * mouseDrag.getX()));
        }
        if (translate) {
            movedObject.transform(new Mat4Transl((editAxis=='x'?1:0)*MOVE_SPEED * mouseDrag.getX(), (editAxis=='y'?1:0)*MOVE_SPEED * mouseDrag.getX(), (editAxis=='z'?1:0)*MOVE_SPEED * mouseDrag.getX()));
        }
        canvas.redraw();
    }

    private void moveCamera(Vec2D mouseDrag) {
        originalCamera = camera;
        if(rotate) {
            canvas.clear();
            camera = originalCamera.withAzimuth(originalCamera.getAzimuth() + mouseDrag.getX() * ROT_SPEED).withZenith(originalCamera.getZenith() + mouseDrag.getY() * ROT_SPEED);
            canvas.getScene().setView(camera.getViewMatrix());
            canvas.redraw();
        }
        if (translate) {
            canvas.clear();
            camera = originalCamera.up(MOVE_SPEED * mouseDrag.getY());
            camera = camera.left(MOVE_SPEED * mouseDrag.getX());
            canvas.getScene().setView(camera.getViewMatrix());
            canvas.redraw();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        rotate = false;
        translate = false;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        originalCamera = camera;
        if(e.getUnitsToScroll() < 0) {
            canvas.clear();
            camera = originalCamera.forward(SCROLL_SPEED);
            canvas.getScene().setView(camera.getViewMatrix());
            canvas.redraw();
        } else if (e.getUnitsToScroll() > 0) {
            canvas.clear();
            camera = originalCamera.backward(SCROLL_SPEED);
            canvas.getScene().setView(camera.getViewMatrix());
            canvas.redraw();
        }
        originalCamera = camera;
    }

    public void setEditAxis(char editAxis) {
        this.editAxis = editAxis;
    }
}
